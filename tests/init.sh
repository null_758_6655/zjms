#!/usr/bin/env bash

sed -i.bak '/export ZJ_PATH=/d' ~/.bashrc
sed -i.bak '/export LD_LIBRARY_PATH=/d' ~/.bashrc
sed -i.bak '/export LC_ALL=/d' ~/.bashrc

echo "export LC_ALL=en_US.UTF-8" >> ~/.bashrc
echo "export ZJ_PATH=${HOME}/zj" >> ~/.bashrc

perl -ane 'print "$F[1]:" if /^-L/' ${HOME}/zj/ccpath > /tmp/__z__ldpath
echo "export LD_LIBRARY_PATH=`cat /tmp/__z__ldpath`\${LD_LIBRARY_PATH}" >> ~/.bashrc
rm /tmp/__z__ldpath

. /etc/profile
. ~/.bashrc

curdir=`pwd`

cd ../src
./init.sh
cd $curdir

#################
# build postgres
#################
# On CentOS7, install deps:
#     wget
#     lbzip2
#     zlib-devel
#     readline-devel
#     systemd-devel
Systemd=$1  # 运行环境的1号进程是否是systemd

PGVersion="10.4"
PGPath=${HOME}/.____PostgreSQL
PGBinPath=${PGPath}/bin
PGDataPath=${PGPath}/data

if [[ 0 == `\ls ${HOME}/.pgpass | wc -l` ]]; then
    echo "# hostname:port:database:username:password" > ${HOME}/.pgpass
fi
chmod 0600 ${HOME}/.pgpass

if [[ 0 -eq `\ls -d ${PGPath} | wc -l` ]]; then
    cd ${HOME}
    if [[ 0 -eq `\ls postgresql-${PGVersion}.tar.bz2 | wc -l` ]]; then
        wget https://ftp.postgresql.org/pub/source/v${PGVersion}/postgresql-${PGVersion}.tar.bz2
    fi

    tar -xf postgresql-${PGVersion}.tar.bz2
    if [[ 0 -ne $? ]]; then
        printf "\033[31;01mfailed: tar -xf postgresql-${PGVersion}.tar.bz2 !!\033[00m\n"
        exit 1
    fi

    cd postgresql-${PGVersion}
    if [[ "" != ${Systemd} ]]; then
        ./configure --prefix=${PGPath} --with-systemd
    elif [[ 1 -eq `pidof systemd` ]]; then
        ./configure --prefix=${PGPath} --with-systemd
    else
        ./configure --prefix=${PGPath}
    fi

    make -j `cat /proc/cpuinfo| grep processor | wc -l` && make install
fi

# start postgresql
# PG: 留空表示仅接受本地 UNIX 域套接字连接
sed -i '/#*listen_addresses =/d' ${PGDataPath}/postgresql.conf
echo "listen_addresses = 'localhost'" >> ${PGDataPath}/postgresql.conf

# PG: 以源码根路径作为 UNIX 域套接字存放路径
sed -i '/#*unix_socket_directories =/d' ${PGDataPath}/postgresql.conf
echo "unix_socket_directories = '${HOME}'" >> ${PGDataPath}/postgresql.conf

# PG: UNIX 域套接字权限
sed -i '/#*unix_socket_permissions =/d' ${PGDataPath}/postgresql.conf
echo "unix_socket_permissions = 0700" >> ${PGDataPath}/postgresql.conf

# # PG: 最大连接数
# sed -i '/#*max_connections =/d' ${PGDataPath}/postgresql.conf
# echo "max_connections = 512" >> ${PGDataPath}/postgresql.conf
#
# # PG: 事务锁上限
# sed -i '/#*max_locks_per_transaction =/d' ${PGDataPath}/postgresql.conf
# echo "max_locks_per_transaction = 256" >> ${PGDataPath}/postgresql.conf
#
# # PG: shared buffers siz，设置为总内存的 1/3
# sed -i '/#*shared_buffers =/d' ${PGDataPath}/postgresql.conf
# echo "shared_buffers = $((`free -m | fgrep -i 'mem' | awk -F' ' '{print $2}'` / 3))MB" >> ${PGDataPath}/postgresql.conf
#
# # PG: max_wal_size，设置为总内存的 1/2
# sed -i '/#*max_wal_size =/d' ${PGDataPath}/postgresql.conf
# echo "max_wal_size = $((`free -m | fgrep -i 'mem' | awk -F' ' '{print $2}'` / 2))MB" >> ${PGDataPath}/postgresql.conf
#
# # PG: work_mem，设置为 64MB
# sed -i '/#*work_mem =/d' ${PGDataPath}/postgresql.conf
# echo "work_mem = 64MB" >> ${PGDataPath}/postgresql.conf
#
# # PG: max_stack_depth，设置为系统线程栈的大小 - 1M
# sed -i '/#*max_stack_depth =/d' ${PGDataPath}/postgresql.conf
# echo "max_stack_depth = $((`ulimit -s` / 1024 - 1))MB" >> ${PGDataPath}/postgresql.conf

${PGBinPath}/pg_ctl -D ${PGDataPath} initdb
${PGBinPath}/pg_ctl stop -D ${PGDataPath} -l ${PGDataPath}/log
${PGBinPath}/pg_ctl start -D ${PGDataPath} -l ${PGDataPath}/log
${PGBinPath}/createdb -hlocalhost -O `whoami` zjdb
